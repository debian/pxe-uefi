Here is some documentation related to netboot uefi with pxe
* Tianocore project, framework for UEFI, allow to simulate UEFI in kvm http://www.tianocore.org/contrib/getting-started.html
    * Start using https://github.com/tianocore/tianocore.github.io/wiki/start-using-UEFI#UEFI_compatible_motherboard, look at OVMF with QEMU
* Why PXE with EFI doesn't work well with dnsmasq 
    * DHCP option 43 seems to make some hardware to fail https://forums.fogproject.org/topic/5750/dnsmasq-proxy-booting-with-uefi/30
    * Always about option 43 https://www.mail-archive.com/dnsmasq-discuss@lists.thekelleys.org.uk/msg09233.html
    * DNSmasq manual http://www.thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html
    * DNSmasq versus isc-dhcp server configuration https://wiki.kubuntu.org/UEFI/SecureBoot-PXE-IPv6
* EFI Bootloaders http://www.rodsbooks.com/efi-bootloaders/index.html
* DHCP
    * Options
        * Full list of DHCP boot parameters http://www.iana.org/assignments/bootp-dhcp-parameters/bootp-dhcp-parameters.xhtml
        * Arch option is defined in https://tools.ietf.org/html/rfc4578
        * DHCP Options and BOOTP Vendor Extensions a.k.a 43 extension https://tools.ietf.org/html/rfc2132
    * Isc dhcpd config 
        * Select pxe depending on arch and user-class http://serverfault.com/questions/804750/uefi-ipxe-boot-into-debian-results-in-grub-shell
        * Extended example http://3dstoneage.com/doc/pxe/dhcpd.conf
        * Another example http://www.syslinux.org/archives/2014-October/022683.html
        * ARM example https://www.kraxel.org/blog/2016/11/advanced-network-booting/
        * PPC example in MaaS https://bugs.launchpad.net/maas/+bug/1401841
        * IPV4/IPV6 example https://wiki.kubuntu.org/UEFI/SecureBoot-PXE-IPv6
        * Full Arch list can be found here  http://www.iana.org/assignments/dhcpv6-parameters/dhcpv6-parameters.xhtml#processor-architecture
* Tftpd-hpa related doc http://chschneider.eu/linux/server/tftpd-hpa.shtml
* PXE on UEFI with grub related doc
    * https://forums.fogproject.org/topic/5750/dnsmasq-proxy-booting-with-uefi/30
    * https://wiki.gentoo.org/wiki/GRUB2/Chainloading
    * https://wiki.archlinux.org/index.php/GRUB
    * https://wiki.linaro.org/LEG/Engineering/Kernel/UEFI/UEFI_Network_Booting

I found here https://github.com/openSUSE/kiwi/wiki/Setup-PXE-boot-with-EFI-Using-GRUB2 doc about romfile and net setup :
``` 
/usr/bin/qemu-kvm -bios /usr/share/qemu/ovmf-x86_64.bin \
 -device virtio-net-pci,romfile=,netdev=mynet0,mac=00:12:34:56:78:9a \
 -netdev tap,ifname=tun2,script=/etc/qemu-ifup,id=mynet0 \
 -m 1024
```
How to use romfile is documented here https://github.com/tianocore/edk2/tree/master/OvmfPkg :
```
 -device e1000,...,romfile=/full/path/to/efi-e1000.rom
 -device ne2k_pci,...,romfile=/full/path/to/efi-ne2k_pci.rom
 -device pcnet,...,romfile=/full/path/to/efi-pcnet.rom
 -device rtl8139,...,romfile=/full/path/to/efi-rtl8139.rom
 -device virtio-net-pci,...,romfile=/full/path/to/efi-virtio.rom
```
List of romfile is available on debian/ubuntu running
```
  dpkg -L ipxe-qemu
...
/usr/lib/ipxe/qemu/efi-e1000.rom
/usr/lib/ipxe/qemu/pxe-ne2k_pci.rom
/usr/lib/ipxe/qemu/pxe-eepro100.rom
/usr/lib/ipxe/qemu/efi-virtio.rom
/usr/lib/ipxe/qemu/efi-eepro100.rom
/usr/lib/ipxe/qemu/efi-rtl8139.rom
/usr/lib/ipxe/qemu/pxe-rtl8139.rom
/usr/lib/ipxe/qemu/pxe-pcnet.rom
/usr/lib/ipxe/qemu/efi-ne2k_pci.rom
/usr/lib/ipxe/qemu/pxe-e1000.rom
/usr/lib/ipxe/qemu/pxe-virtio.rom
/usr/lib/ipxe/qemu/efi-pcnet.rom
...
```
* ISC DHCP extra config
    * Be sure that your ISC DHCP server has the "dynamic-bootp" option for the "range" directive http://savannah.gnu.org/bugs/?42631
    * https://dev.gentoo.org/~floppym/grub.html#Networking-commands
* Raspberry PI netboot (usefull to see how to configure with dnsmasq)
    * https://www.raspberrypi.org/documentation/hardware/raspberrypi/bootmodes/net_tutorial.md
    * https://www.raspberrypi.org/blog/pi-3-booting-part-ii-ethernet-all-the-awesome/
* Bridging
    * http://unix.stackexchange.com/questions/159191/setup-kvm-on-a-wireless-interface-on-a-laptop-machine
    * How wlan bridging is different : https://www.virtualbox.org/manual/ch06.html#network_bridged
* Chaining PXE efi important information I also observed when trying to chain
    * Related thread about PXE + UEFI in virtualbox https://forums.virtualbox.org/viewtopic.php?f=1&t=65463 Diego tell us : 

> The capacity to chainload another EFI executable is being developed for ipxe and syslinux. 
> GRUB2 EFI is the only EFI loader with network capacity that I found to be able to chainload 
> another EFI executable, unfortunately cannot initiate network access in most machines 
> when executed locally (it works perfectly when executed from network)

* Chain GRUB and windows: https://wiki.archlinux.org/index.php/GRUB#Windows_installed_in_UEFI-GPT_Mode_menu_entry
* Fix UEFI problems
    * http://www.rodsbooks.com/refind/getting.html
    * https://doc.ubuntu-fr.org/boot-repair
* Distro related doc
    * https://wiki.ubuntu.com/UEFI/PXE-netboot-install
    * https://wiki.debian.org/GrubEFIReinstall
    * https://wiki.debian.org/UEFI
* SYSLINUX/PXELINUX Bootloader http://www.syslinux.org/wiki/index.php?title=PXELINUX
* Unclassified
    * http://doc.rogerwhittaker.org.uk/ipxe-installation-and-EFI/
    * http://mjg59.livejournal.com/138188.html
